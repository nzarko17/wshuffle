#!/usr/bin/python3

import sys
import io
import random
from pathlib import Path  # for is_file function
import os
import argparse

from revisionbuilder import RevisionBuilder
from shufflebase import WShuffle, show_info
from editorproc import WinEditorProc, PosixEditorProc
from zutils import get_os, init_clargs, show_zero_or_negative_warning

if __name__ == '__main__':
    if len(sys.argv) == 1:
        executable = os.path.basename(sys.argv[0])
        print("Usage: [python] {} filename".format(executable))
        exit(-1)
    else:
        rev_builder = RevisionBuilder()
        rev_builder.create()

        parser = init_clargs()
        args = parser.parse_args()       

        # input_filename = sys.argv[-1]
        # print('Reading {}'.format(sys.argv[-1]))
        input_filename = args.filename
        print('Reading {}'.format(input_filename))

        shuffler = WShuffle(file_in=input_filename)
        file_path = ''
        
        if args.lines_between > 0:
            print('Lines between argument turned on with value : {} '.format(args.lines_between))
            shuffler.setlinesbetween(args.lines_between)
        else:
            # print("Trying to set zero or negative value for lines_between ignoring.\nReverting to default.")
            show_zero_or_negative_warning('lines_between')


        if args.repetitions > 0:
            print('repetitions argument turned on with value : {} '.format(args.repetitions))
            shuffler.setrepetitions(args.repetitions)  
        else:
            show_zero_or_negative_warning('repetitions')  

        if args.ns:
            print('ns option turned on!\n')
            file_path = shuffler.ncopies()
        else:
            file_path = shuffler.shuffle()
        # file_path = shuffler.ncopies()

        if get_os()['OS_Name'] == 'Windows':
            editor = WinEditorProc(filename=file_path)
        else:
            editor = PosixEditorProc(filename=file_path)

        show_info(editor)
        res = editor.start_editor()
        print('Process {} exited with exit code {}'.format(editor.editor(), res))
