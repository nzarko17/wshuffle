
import os
from pathlib import Path
from zutils import get_os, get_path, getsettingsfile


class RevisionBuilder:
    """
    RevisionBuiler : creates revision files from existing words files.
    We can use this class in two ways.
    """
    '''
    :type filelist :  a list containing the files to include in revision 
    '''
    filelist = []

    def __init__(self,data_dir=os.getcwd(),
                 from_to=[1,2,3], filelist=None,
                 split=False):
        """

        :type from_to: list of lessons numbers to include in revision
        :type filelist: list of filenames with full path to include in revision
        if naming conversion not followed.
        """
        self.datadir = data_dir
        self.fromto = from_to
        self.split = split
        if filelist is not None:
            self.filelist = filelist

    def create(self):
        prefix = ['lesson_']*len(self.fromto)
        for i in range(len(prefix)):
            prefix[i] += str(self.fromto[i])

        files = os.listdir(self.datadir)
        # print('Files in {} directory\n{}'.format(self.datadir,files))
        for file in files:
            if file.startswith(tuple(prefix)):
                self.filelist.append(file)

        print('Files in revision {} '.format(self.filelist))

        print(prefix)
