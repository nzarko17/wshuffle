import platform
import os
import argparse
from pathlib import Path

def get_os():
    os_info = {}
    os_info['Platform'] = platform.platform()
    os_info['OS_Name'] = platform.system()
    os_info['Version'] = platform.version()
    return os_info

def get_path():
    return os.environ['PATH']

def appdir():
  return os.path.dirname(os.path.realpath(__file__))

def getsettingsfile():
  s_path = Path(appdir() + '/' + 'settings.json')
  return s_path

def findFile(seekName, path, implicitExt=''):
    """Given a pathsep-delimited path string, find seekName.
    Returns path to seekName if found, otherwise None.
    Also allows for files with implicit extensions (eg, .exe), but
    always returning seekName as was provided.
    >>> findFile('ls', '/usr/bin:/bin', implicitExt='.exe')
    '/bin/ls'
    """
    if (os.path.isfile(seekName)
          or implicitExt and os.path.isfile(seekName + implicitExt)):
        # Already absolute path.
        return seekName
    for p in path.split(os.pathsep):
        candidate = os.path.join(p, seekName)
        if (os.path.isfile(candidate)
              or implicitExt and os.path.isfile(candidate + implicitExt)):
            return candidate
    return None


def init_clargs():
  parser = argparse.ArgumentParser()
  parser.add_argument("--ns", help="Create a formated copy of document (no shuffle)",
                      action="store_true")
  parser.add_argument("filename", help="The file name to hadle")
  parser.add_argument("-b","--lines_between", type=int,
                      help="How many lines between the shuffles. Default=3",nargs="?", default=3)
  parser.add_argument("-r", "--repetitions", type=int, nargs="?", default=5,
                      help="How many repetitions will shuffler do. Default=5.")
  return parser

def show_zero_or_negative_warning(arg):
  message = '''Trying to set zero or negative value to {} argument. Ignoring!\n
    Reverting to default!'''.format(arg)
  print(message)