import io
import random
from pathlib import Path  # for is_file function

from editorproc import WinEditorProc, PosixEditorProc
from zutils import get_os, init_clargs, show_zero_or_negative_warning

# from PyQt5.QtPrintSupport import QPrintDialog, QPageSetupDialog, QPrintPreviewDialog, QPrinter,QPrintPreviewWidget
# from PyQt5.QtWidgets import QApplication


class WShuffle:
    lines = 3
    filename = ''
    out_filename = ''
    wordslist = []
    repNum = 3

    def __init__(self, file_in, repetitions=5, linesbetween=3):
        self.lines = linesbetween
        self.repNum = repetitions
        self.filename = file_in
        self.out_filename = file_in[:-4] + '_out.txt'

    def setlinesbetween(self,lb):
        self.lines = lb

    def setrepetitions(self, rn):
        self.repNum = rn
    
    def write(self):
        self.writeToFile(self.filename)

    def writeToFile(self, filename):
        try:
            contents = []
            with io.open(filename, 'w', encoding='utf8') as f:
                for wlist in self.wordslist:
                    count = 1
                    for s in wlist:
                        word = '{:4d}'.format(count) + '. ' + s + '\n'
                        # print(word)
                        # f.write(word)
                        contents.append(word)
                        count += 1

                    for i in range(self.lines-1):
                        contents.append('\n')
                        # f.write("\n")
                # Removes the last empty lines from last word list
                to_write = ''.join(contents).rstrip()
                f.write(to_write)
        except FileNotFoundError as fnf:
            print('Σφάλμα τύπου ', fnf, 'Δεν υπάρχει τέτοιο αρχείο.')
        except IOError as ioe:
            print('Σφάλμα Τύπου ',ioe, 'Λάθος κατά την εγγραφή στο αρχείο!')

    def readFirstNLines(self, filename, N=None):
        contents = []
        try:
            print('Reading {}'.format(filename))
            with open(filename, encoding='utf8') as myfile:
                contents = myfile.read().splitlines()

            if N is not None:
                if len(contents) >= N:
                    self.wordslist = contents[:N]
                    return self.wordslist
                else:
                    print('Index out of bounds!')
                    return []
            else:
                self.wordslist = contents[:]
                return self.wordslist

                # head = [next(myfile) for x in range(N)]
        except FileNotFoundError as fnf:
            print('Σφάλμα τύπου ', fnf, 'Δεν υπάρχει τέτοιο αρχείο.')

    def read(self, lines=None):
        return self.readFirstNLines(self.filename, lines)

    def formatWords(self):
        equalto = '->_______________________'
        sz = [len(w) for w in self.wordslist]
        max_len = max(sz)
        self.wordslist = [w + (max_len - len(w) + 2) * '.' + equalto for w in self.wordslist]
        return self.wordslist

    def ncopies_from_file(self, filename, lines=None):
        wl = []
        self.wordslist = self.readFirstNLines(self.filename, lines)
        self.wordslist = self.formatWords()
        self.wordslist = [s + '\n' for s in self.wordslist]
        for i in range(self.repNum):
            # random.shuffle(self.wordslist)
            wl.append(self.wordslist[:])
            # wl.append(self.lines*'\n')

        self.wordslist = wl[:]
        self.writeToFile(self.out_filename)

        out_path = Path('./' + self.out_filename)
        print('Contents saved to ', out_path.absolute())
        return out_path

    def shuffle_from_file(self, filename, lines=None):
        wl = []
        self.wordslist = self.readFirstNLines(self.filename, lines)
        self.wordslist = self.formatWords()
        self.wordslist = [s + '\n' for s in self.wordslist]
        for i in range(self.repNum):
            random.shuffle(self.wordslist)
            wl.append(self.wordslist[:])
            # wl.append(self.lines*'\n')

        # del wl[-1]
        self.wordslist = wl[:]
        self.writeToFile(self.out_filename)

        out_path = Path('./' + self.out_filename)
        print('Contents saved to ', out_path.absolute())
        return out_path

    def shuffle(self, lines=None):
        return self.shuffle_from_file(self.filename, lines)

    def ncopies(self, lines = None):
        return self.ncopies_from_file(self.filename, lines)


def show_info(editor):
    info = get_os()
    message  = f'''OS : {info['OS_Name']}\nVersion : {info['Version']}\nEditor Class: {type(
        editor)}\nEditor name: {editor.editor()}'''
    print(message)
