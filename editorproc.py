import json
import subprocess as sp
from shutil import which
from zutils import appdir,getsettingsfile

# from zutils import get_os, get_path, findFile

# print('OS : ', get_os()['OS_Name'])
# print('PATH : ', get_path())
# print(findFile('mongo.exe',get_path()))


# Editor process. Try to open the produced file
# to editor of our choise (default : kate)
class EditorProc:

    editor_name = 'kate'
    filename = ''
    editor_info = {}

    def __init__(self, editor_name='kate', filename=''):
        self.editor_name = editor_name
        self.filename = filename

    def start_editor(self, editor_name='kate'):
        try:
            res = sp.check_call([editor_name, "{}".format(self.filename)])
            return res
        except sp.CalledProcessError as ce:
            err_str = '''Subprocess {} returned an error : {}. Check if {}
            is in PATH system variable'''.format(ce.output, editor_name, editor_name)
            print(err_str)
            print('Error details : ')
            if ce.output.startswith('error: {'):
                error = json.loads(ce.output[7:])  # Skip "error: "
                print(error['code'])
                print(error['message'])
                return -1

    def editor(self):
        return self.editor_name

    def read_settings(self):
        print('Try to read setting file...',end='')
        try:
            with open(getsettingsfile(), 'r', encoding='utf-8') as sf:
                self.editor_info = json.load(sf)
            print('OK',end='\n')
            return self.editor_info
        except FileNotFoundError as fnfe :
            print('Σφάλμα τύπου ', fnfe, 'Δεν υπάρχει τέτοιο αρχείο.')
            return None

    def write_settings(self):
        pass


# WinEditorProc class
# Handles Windows specific editors (default notepad++)
class WinEditorProc(EditorProc):

    editor_name = 'notepad++'
    filename = None

    def __init__(self,  editor_name='notepad++', filename=''):
        super().__init__(editor_name, filename)

    def start_editor(self, editor_name='notepad++'):
        self.editor_info = self.read_settings()
        if self.editor_info is not None:
            self.editor_name = self.editor_info['editor_exec']
            return super().start_editor(self.editor_name)

    def editor(self):
        return self.editor_name


# PosixEditorProc class
# Handles Posix systems specific editors (default : kate)
class PosixEditorProc(EditorProc):

    editor_name = 'kate'
    filename = None

    def __init__(self,  editor_name='kate', filename=''):
        super().__init__(editor_name, filename)

    def start_editor(self, editor_name='kate'):
        self.editor_name = which(editor_name)
        if self.editor_name is not None:
            res = super().start_editor(self.editor_name)
            return res
        else:
            print('Can not found executable for {}! Use your package manager to install it!!'.format(editor_name))
            return -1

    def editor(self):
        return self.editor_name

