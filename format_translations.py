# format_translations.py
import argparse
import sys
import os

def init_clargs() :
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="The file name to hadle")

    return parser


def formatWords(filename):
    result = []
    with open(filename, 'r', encoding='utf-8') as f:
        wordslist = f.readlines()
    
    print(wordslist)

    for x in wordslist:
        if ('.' in x) and ('->' in x):
            words = x.split('->')
            if len(words) > 0:
                result.append(words[0])

    wordscopy = result[:]
    equalto = '->_______________________'
    sz = [len(w) for w in wordscopy]
    max_len = max(sz)
    wordslist = [w + (max_len - len(w) + 2) * '.' + equalto for w in wordscopy]
    return wordslist


if __name__ == '__main__':
    if len(sys.argv) == 1:
        executable = os.path.basename(sys.argv[0])
        print("Usage: [python] {} filename".format(executable))
        exit(-1)
    else:
        parser = init_clargs()
        args = parser.parse_args()    

        input_filename = args.filename
        print('Reading {}'.format(input_filename))

        words = formatWords(input_filename)
        # Remove duplicates
        words = list(dict.fromkeys(words))
        out_filename = input_filename[:-4] + '_out.txt'

        with open(out_filename, 'w', encoding='utf-8') as f:
            for x in words:
                f.write(x)
                f.write('\n\n')
        

